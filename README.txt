StateServ - UDP server & client for sending the state of a machine over the network

Server needs a static IP - client pings with magic packet to request a reply.

State includes cpu usage, memory usage, network traffic, etc. as CSV
