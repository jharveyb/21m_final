#!/usr/bin/python3
# UDP client - Pings server with static magic packet
# and sends response along to Max/MSP
# server IP is $1, port is $2, polling frequency is $3 - all required

# based on pymotw.com/3/socket/udp.html

import socket
import csv
from time import sleep
from hashlib import blake2b
from sys import argv
from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse

osc_startup()
osc_udp_client("127.0.0.1", 2222, "max")

address = argv[1]
port = int(argv[2])
frequency = int(argv[3])
hash = blake2b(digest_size=32)
hash.update(b'21M.080 Final Project')
print('magic hash is {}'.format(hash.hexdigest()))
# Create socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = (address, port)
print('listening for server @ {}, port {}'.format(address, port))

message=hash.hexdigest()
print('polling @ {} Hz'.format(frequency))
frequency = 1/frequency
data_list = []

while True:
    # Send; use encode to convert from string to bytes
    sock.sendto(message.encode(), server_address)

    # Receive
    sleep(frequency)
    data, server = sock.recvfrom(4096)
    #print('received {!r}'.format(data))
    # Decode packet into original list
    for value in csv.reader([data.decode("utf-8")]):
        data_list = value
    # Wrap value in OSC message and send to Max/MSP
    cpumsg = oscbuildparse.OSCMessage("/cpu", ",f", [data_list[0]])
    osc_send(cpumsg, "max")
    osc_process()
    memmsg = oscbuildparse.OSCMessage("/mem", ",f", [data_list[1]])
    osc_send(memmsg, "max")
    osc_process()
    sentmsg = oscbuildparse.OSCMessage("/sent", ",f", [data_list[2]])
    osc_send(sentmsg, "max")
    osc_process()
    recvmsg = oscbuildparse.OSCMessage("/recv", ",f", [data_list[3]])
    osc_send(recvmsg, "max")
    osc_process()

osc_terminate()