#!/usr/bin/python3
# UDP server - Measures machine state, caches,
# and sends cached state in response to magic packet
# listening IP is $1, port is $2 - both required

# based on pymotw.com/3/socket/udp.html

import socket
import psutil
from collections import deque
from hashlib import blake2b
from sys import argv

def build_packet(state_smooth):
    packet = ""
    packet += str(state_smooth['cpu'])
    packet += ","
    packet += str(state_smooth['mem'])
    packet += ","
    packet += str(state_smooth['sent'])
    packet += ","
    packet += str(state_smooth['recv'])
    return packet

def moving_average(values, new_value, precision):
    values.append(new_value)
    average = round(sum(values) / len(values), precision)
    return average

# measure the system; pass empty list,
# return filled as [CPU load, memory usage, sent bytes, received bytes]
def system_measure(state_cache, state_smooth, net_state, precision):
    cpu = psutil.cpu_percent()
    while (cpu == 0.0):
        cpu = psutil.cpu_percent()
    state_smooth['cpu'] = moving_average(state_cache['cpu'], cpu, precision)
    mem = psutil.virtual_memory()
    mem = mem[1]/mem[0]
    mem = 1 - mem
    mem = round(mem, 4)
    state_smooth['mem'] = moving_average(state_cache['mem'], mem, precision)
    net = psutil.net_io_counters()
    # Network values are totals vs. immediate, so much cache-and-compare
    if (net_state[0] == 0 & net_state[1] == 0):
        net_state[0] = net[0]
        net_state[1] = net[1]
        net = psutil.net_io_counters()
    sent = net[0] - net_state[0]
    recv = net[1] - net_state[1]
    net_state[0] = net[0]
    net_state[1] = net[1]
    state_smooth['sent'] = moving_average(state_cache['sent'], sent, precision)
    state_smooth['recv'] = moving_average(state_cache['recv'], recv, precision)
    return

address = argv[1]
port = int(argv[2])
hash = blake2b(digest_size=32)
hash.update(b'21M.080 Final Project')
print('magic hash is {}'.format(hash.hexdigest()))
# Create socket & bind to port
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = (address, port)
print('starting UDP server, {} port {}'.format(*server_address))
sock.bind(server_address)

# Initialize buffers for system measurement
net_state = [0, 0]
buffer_depth=25
precision = 3
state_cache = {'cpu': deque(maxlen=buffer_depth), 'mem': deque(maxlen=buffer_depth),
                'sent': deque(maxlen=buffer_depth), 'recv': deque(maxlen=buffer_depth)}
state_smooth = {'cpu': 0.0, 'mem': 0, 'sent': 0, 'recv': 0}
out_buffer = ""

while True:
    # Receive
    data, client_address = sock.recvfrom(4096)
    system_measure(state_cache, state_smooth, net_state, precision)

    # Send; use encode to convert from string to bytes
    if (data == hash.hexdigest().encode()):
        out_buffer = build_packet(state_smooth)
        sock.sendto(out_buffer.encode(), client_address)
        #print('server sent {} back to {}'.format(out_buffer, client_address))